import axios from 'axios';

export const imageUpload = (file, onUploadProgress) => {
  let formData = new FormData();
  formData.append("file", file);
  return axios.post('/api/books/save-image', formData, {
  })
    .then(res => {
      console.log("response:", res)
      return res
    })
    .catch(err => {
      console.log("err:", err)
    });
};

export const getProfileImage = () => {
  axios.get('/api/books/get-profile-image')
    .then(res => {
      console.log("response:", res)
      return res
    })
    .catch(err => {
      console.log("err:", err)
    });
};

