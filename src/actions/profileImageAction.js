import {SET_PROFILE_IMAGE} from "./types"

export let setProfileImageMethod = (data) => {
    return {
        type: SET_PROFILE_IMAGE,
        payload: data
    }
}