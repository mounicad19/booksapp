import axios from 'axios';

export const getBookById = (id) => {
    return axios.get(`/api/books/get-book-by-id/${id}`)
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log("err:", err)
        });
}