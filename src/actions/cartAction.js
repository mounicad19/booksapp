import axios from 'axios';

export const getAllCartItems = () => {
    return axios.get(`/api/cart/get-all`)
        .then(cartResponse => {
            return cartResponse
        })
        .catch(err => {
            console.log("err:", err)
        });
}

export const updateCartquantity = (id, operation) => {    
    return axios.patch(`/api/cart/updateQuantity/${operation}/${id}`, )
        .then(cartResponse => {
            return cartResponse.data
        })
        .catch(err => {
            console.log("err:", err)
        });
}

export const removeProductFromCart = (id) => {    
    return axios.get(`/api/cart/delete/${id}`, )
        .then(cartResponse => {
            return cartResponse.data
        })
        .catch(err => {
            console.log("err:", err)
        });
}

export const addToCart = (productData) => {    
    return axios.post(`/api/cart/add`, productData)
        .then(cartResponse => {
            console.log("response for addToCart:", cartResponse);
            return cartResponse.data
        })
        .catch(err => {
            console.log("err:", err)
        });
}


