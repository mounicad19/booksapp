import axios from 'axios';
import jwt_decode from 'jwt-decode';
import { useNavigate  } from 'react-router-dom';
import { useDispatch } from "react-redux";
import setAuthToken from '../setAuthToken';
import { GET_ERRORS, SET_CURRENT_USER } from './types';


// export const registerUser = (user) => {
//     let dispatch = useDispatch();
//     const navigate = useNavigate();
//     axios.post('/api/users/register', user)
//         .then(res => navigate('/login'))
//         .catch(err => {
//             dispatch({
//                 type: GET_ERRORS,
//                 payload: err.response.data
//             });
//         });
// }

//MOVE THESE FUNCTIONS TO LOGIN COMPONENT============> ********
// export const loginUser = (user) => {
//     let dispatch = useDispatch();
//     const navigate=useNavigate()
//     axios.post('/api/users/login', user)
//         .then(res => {
//             console.log("res.data:", res)
//             const { token } = res.data;
//             localStorage.setItem('jwtToken', token);
//             setAuthToken(token);
//             const decoded = jwt_decode(token);
//             dispatch(setCurrentUser(decoded));
//             navigate("/")
//         })
//         .catch(err => {
//             dispatch({
//                 type: GET_ERRORS,
//                 payload: err.response.data
//             });
//         });
// }

// export const setCurrentUser = decoded => {
//     return {
//         type: SET_CURRENT_USER,
//         payload: decoded
//     }
// }


// export const logoutUser = () => {
//     // let dispatch = useDispatch();
//     // const navigate = useNavigate();
//     localStorage.removeItem('jwtToken');
//     setAuthToken(false);
//     dispatch(setCurrentUser({}));
//     navigate('/login');
// }