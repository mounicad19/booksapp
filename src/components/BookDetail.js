import styled from "styled-components";
import React, { useState, useEffect, useReducer } from "react";
import { useLocation, Link } from "react-router-dom";

import avatar from '../assets/avatar.png';
import AudioPlayer from './AudioPlayer';
import tracks from "./tracks";
import { getBookById } from "../actions/detailsAction";
import { addToCart } from "./../actions/cartAction";
import { useNavigate } from "react-router-dom";

const BookDetail = () => {
    const { detailsData } = useLocation();
    const navigate = useNavigate()
    const [bookDetails, setBookDetails] = useState({})
    const [cartObj, setCartObj] = useState({})
    let location = useLocation();
    const [flag, setFlag] = useState(false)

    let backButton = () => {
        navigate(-1)
    }

    useEffect(() => {
        fetchBookById()
    }, [])

    let fetchBookById = async () => {
        let idVal = location.pathname.split('/')
        let details = await getBookById(idVal[2])
        setBookDetails(details)
    }

    let addToCartFn = async (item) => {
        let requiredObj = {
            title: item.title,
            author: item.author,
            price: "20",
            quantity: item.quantity,
            createdAt: item.createdAt,
            updatedAt: item.updatedAt
        }
        await addToCart(requiredObj)
        setFlag(true)
    }

    return (
        <>

            <h1>book detail screen</h1>
            <div>
                <div >
                    <i onClick={backButton} className="fa fa-arrow-circle-left fa-lg"
                        style={{ cursor: "pointer" }}>Back</i>
                </div>
            </div>
            <Container>
                <ImageStyle alt="" src={avatar} />
                <div>
                    {bookDetails?.title}
                </div>
                <p>{bookDetails?.author}</p>

                <button style={{
                    background: "cadetblue",
                    border: '1px solid black'
                }}
                    onClick={() => !flag && addToCartFn(bookDetails)}
                >
                    {flag ?
                        <Link to={{ pathname: `/cart` }} > <i className="fa fa-arrow-right fa-lg"
                            style={{ cursor: "pointer" }}> Go to Bag</i></Link>
                        : "Add to Cart"}
                </button>
                <AudioPlayer tracks={tracks} />
            </Container>
        </>
    )
}

const Container = styled.div`
padding: 2px 16px;
cursor:pointer
`;

const ImageStyle = styled.img`
height: auto;
width: 20%;
cursor: pointer;
`;

const AddToCartButton = styled.div`
width: 10%;
background: blue;
color: white
`

export default BookDetail

