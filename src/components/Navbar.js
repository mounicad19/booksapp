// Navbar.js

import React, { Fragment, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { useDispatch, useSelector } from "react-redux";
import Dropdown from 'react-bootstrap/Dropdown';
import { Button } from 'react-bootstrap';
import setAuthToken from '../setAuthToken';
import { SET_CURRENT_USER } from '../actions/types';

const Navbar = (props) => {
    let { info } = props;
    console.log("props:", props)
    const navigate = useNavigate();
    let dispatch = useDispatch();
    let userDetails = useSelector(state => state.auth);
    let imageDetails = useSelector(state => state.imageDetails);

    const [profileImage, setProfileImage] = useState(imageDetails && imageDetails?.img ? imageDetails.img : null);

    useEffect(() => {
        setProfileImage(imageDetails.img)
    });

    let setCurrentUserFunc = decoded => {
        return {
            type: SET_CURRENT_USER,
            payload: decoded
        }
    }

    const onLogout = (e) => {
        e.preventDefault();
        localStorage.removeItem('jwtToken');
        setAuthToken(false);
        dispatch(setCurrentUserFunc({}));
        navigate('/login');
    }

    let renderImage = () => {
        return (
            <Link to="/profile">
                <img
                    src={profileImage ? URL.createObjectURL(profileImage) : userDetails?.user?.avatar} alt={userDetails?.user?.name}
                    title={userDetails?.user?.name}
                    className="rounded-circle"
                    style={{
                        width: '45px', height: 40, cursor: "pointer",
                        marginLeft: "6px",
                        marginTop: "1rem",
                        marginRight: "6px"
                    }} />
            </Link>
        )
    }

    let authVar;
    if (userDetails?.isAuthenticated) {
        authVar = <Fragment>
            {renderImage()}
            <span style={{ marginTop: '1.5rem' }}>Hello {userDetails?.user?.name}!</span>
            {userDetails?.isAuthenticated ? <i className="fa fa-power-off fa-lg"
                style={{
                    marginTop: '1.6rem',
                    marginLeft: '20px'
                }}
                onClick={onLogout}></i> : null}
        </Fragment>
    } else {
        authVar = <Fragment>
            <Link to="/profile" style={{
                width: "3rem",
                marginTop: "1rem",
                marginLeft: "1rem"
            }}>
                <i className="fa fa-user fa-lg" />
            </Link>
            <Dropdown style={{ marginTop: "10px" }}>
                <Dropdown.Toggle id="dropdown-basic">
                    Log In
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    <Dropdown.Item href="/login">Sign In</Dropdown.Item>
                    <Dropdown.Item href="/register">Register</Dropdown.Item>
                    <Dropdown.Item href="#/action-3">Wishlist</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </Fragment>
    }

    return (
        <div style={{
            background: "#aac5bb",
            height: 80,
            borderBottom: "1px solid black",
            display: "flex"
        }}>
            <div style={{ width: "80%" }} >
                <Link to="/" style={{ color: "white" }}>thrift<b>books</b></Link>
            </div>
            <div style={{ width: "20%", background: "cadetblue", display: "flex" }}>

                {authVar}
                <Link to="/cart" style={{
                    color: "black",
                    marginLeft: '15px',
                    marginTop: '1.2rem',
                    textDecoration: "none"
                }}><i className="fa fa-shopping-cart fa-2x" /> </Link>


            </div>
            {info && <h1>  {info} </h1>}
        </div>
    )
}

const RightDiv = styled.div`
position: absolute;
right: 10px;
margin-top:30px;
`;

const NavbarDiv = styled.div`
position: relative;
display: flex;
flex-wrap: wrap;
align-items: center;
justify-content: space-between;
padding: 1.5rem;
background: cadetblue;
color: white;
flex-wrap: nowrap;
justify-content: flex-start;
`;

const NavBarLink = styled(Link)`
padding-top: .3125rem;
padding-bottom: .3125rem;
margin-right: 1rem;
font-size: 1.25rem;
text-decoration: none;
white-space: nowrap;
color: white;
`;

const DropBtn = styled(Button)`
background-color: pink;
`

export default Navbar;
