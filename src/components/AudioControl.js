import React from "react";
import "./AudioPlayer.css";

const AudioControls = ({
  isPlaying,
  onPlayPauseClick,
  onPrevClick,
  onNextClick
}) => (
  <div className="audio-controls">
    {isPlaying ? (
      <i className="fa fa-pause-circle pauseButtonSize" onClick={() => onPlayPauseClick(false)} />
    ) : (
        <i className="fa fa-play pauseButtonSize"  onClick={() => onPlayPauseClick(true)}/> 
    )}
  </div>
);

export default AudioControls;
