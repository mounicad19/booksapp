import React, { useState, useEffect, useRef } from "react";
import styled from 'styled-components';
import AudioControls from "./AudioControl";
import Backdrop from "./BackDrop";
import "./AudioPlayer.css";

const AudioPlayer = ({ tracks }) => {
  // State
  const [trackIndex, setTrackIndex] = useState(0);
  const [trackProgress, setTrackProgress] = useState(0);
  const [isPlaying, setIsPlaying] = useState(false);

  // Destructure for conciseness
  const { title, artist, color, audioSrc } = tracks[trackIndex];

  // Refs
  const audioRef = useRef(new Audio(audioSrc));
  const intervalRef = useRef();
  const isReady = useRef(false);

  // Destructure for conciseness
  const { duration } = audioRef.current;

  const currentPercentage = duration
    ? `${(trackProgress / duration) * 100}%`
    : "0%";
  const trackStyling = `
      -webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(${currentPercentage}, #fff), color-stop(${currentPercentage}, #777))
    `;

  const startTimer = () => {
    // Clear any timers already running
    clearInterval(intervalRef.current);

    intervalRef.current = setInterval(() => {
      // if (audioRef.current.ended) {
      //   // toNextTrack();
      //   console.log("track has ended")
      // } else {
      if (!audioRef.current.ended) {
        setTrackProgress(audioRef.current.currentTime);
      }
      // }
    }, [1000]);
  };

  const onScrub = (value) => {
    // Clear any timers already running
    clearInterval(intervalRef.current);
    audioRef.current.currentTime = value;
    setTrackProgress(audioRef.current.currentTime);
  };

  const onScrubEnd = () => {
    // If not already playing, start
    if (!isPlaying) {
      setIsPlaying(true);
    }
    startTimer();
  };

  // const toPrevTrack = () => {
  //   if (trackIndex - 1 < 0) {
  //     setTrackIndex(tracks.length - 1);
  //   } else {
  //     setTrackIndex(trackIndex - 1);
  //   }
  // };

  // const toNextTrack = () => {
  //   if (trackIndex < tracks.length - 1) {
  //     setTrackIndex(trackIndex + 1);
  //   } else {
  //     setTrackIndex(0);
  //   }
  // };

  useEffect(() => {
    if (isPlaying) {
      audioRef.current.play();
      startTimer();
    } else {
      audioRef.current.pause();
    }
  }, [isPlaying]);

  // Handles cleanup and setup when changing tracks
  useEffect(() => {
    audioRef.current.pause();

    audioRef.current = new Audio(audioSrc);
    setTrackProgress(audioRef.current.currentTime);

    if (isReady.current) {
      audioRef.current.play();
      setIsPlaying(true);
      startTimer();
    } else {
      // Set the isReady ref as true for the next pass
      isReady.current = true;
    }
  }, [trackIndex]);

  useEffect(() => {
    // Pause and clean up on unmount
    return () => {
      audioRef.current.pause();
      clearInterval(intervalRef.current);
    };
  }, []);

  return (
    <div className="audio-player">
      <div className="track-info">
        {/* <img
            className="artwork"
            src={image}
            alt={`track artwork for ${title} by ${artist}`}
          /> */}
        {/* <h2 className="title">{title}</h2>
          <h3 className="artist">{artist}</h3> */}
        <AudioControls
          isPlaying={isPlaying}
          // onPrevClick={toPrevTrack}
          // onNextClick={toNextTrack}
          onPlayPauseClick={setIsPlaying}
        />
        {/* <input
          type="range"
          value={trackProgress}
          step="1"
          min="0"
          max={duration ? duration : `${duration}`}
          className="progress"
          onChange={(e) => onScrub(e.target.value)}
          onMouseUp={onScrubEnd}
          onKeyUp={onScrubEnd}
          style={{ background: trackStyling }}
        /> */}
        <input type="range" min="1" max="100"
          onChange={(e) => onScrub(e.target.value)}
          value={trackProgress} className="slider" />
        {/* <div className="bar">
          <span className="bar__time">{formatDuration(audioRef.current.currentTime)}</span>
          <div
            className="bar__progress"
            style={{
              background: `linear-gradient(to right, orange ${currentPercentage}%, white 0)`
            }}
            onMouseDown={e => handleTimeDrag(e)}
          >
            <span
              className="bar__progress__knob"
              style={{ left: `${currentPercentage - 2}%` }}
            />
          </div>
          <span className="bar__time">{formatDuration(duration)}</span>
        </div> */}
      </div>
      {/* <Backdrop
          trackIndex={trackIndex}
          activeColor={color}
          isPlaying={isPlaying}
        /> */}
    </div>
  );
};

export default AudioPlayer;

const StyledSlider = styled.input`
input[type="range"]::-webkit-slider-runnable-track {
  background: tomato;
  height: 5px;
 }
 
 input[type="range"]::-moz-range-track {
  background: tomato;
  height: 5px;
 }

 input[type="range"]::-webkit-slider-thumb {
  -webkit-appearance: none;
  height: 15px;
  width: 15px;
  background: pink;
  margin-top: -5px;
  border-radius: 50%;
 }
 
 input[type="range"]::-moz-range-thumb {
  height: 15px;
  width: 15px;
  background: pink;
  margin-top: -5px;
  border-radius: 50%;
 }
`
