// import imgSrc from "./assets/artwork.jpg";
// import imgSrc2 from "./assets/artwork2.jpg";
// import imgSrc3 from "./assets/artwork3.jpg";
import fifty from "../assets/love-freindship_2.mp3";
import iwonder from "../assets/love-freindship_1.mp3";

// All of these artists are at https://pixabay.com/music/search/mood/laid%20back/
export default [  
  {
    title: "50",
    artist: "tobylane",
    audioSrc: fifty,
    // image: imgSrc2,
    color: "#ffb77a"
  },
  {
    title: "I Wonder",
    artist: "DreamHeaven",
    audioSrc: iwonder,
    // image: imgSrc3,
    color: "#5f9fff"
  }
];
