
import React, { useState, useEffect, useRef } from "react";
import { useDispatch } from "react-redux"
import _ from 'lodash';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import avatar from '../assets/Redwall.jpg'
import './carousel.css';

const slidesRange = 6

const CarouselComponent = (props) => {
    const dragItem = useRef();
    const dragOverItem = useRef();

    let { booksProps, carkey, onAddToCart } = props
    var booksData = booksProps
    const [booksJson, setBooksJson] = useState([].concat(booksProps))
    const [slidesData, setSlidesData] = useState([].concat(booksProps))
    const [slideIndex, setSlideIndex] = useState(0)
    const [details, setDetails] = useState({})
    const [CarKey, setCarKey] = useState(carkey)
    const dispatch = useDispatch()

    useEffect(() => {
        setCarKey(!CarKey)
    }, [])

    useEffect(() => {
        const getSlidesNum = () => {
            const tempArr = slidesData.slice()
            let data = tempArr.slice(slideIndex, slidesRange)
            setSlidesData(data)
            setSlideIndex(slideIndex + slidesRange)
        }
        getSlidesNum();
    }, [])

    const goToPrevSlide = () => {
        let booksDataClone = booksData.slice()
        if (slideIndex > 6) {
            if (slideIndex % 6 === 0) {
                let data, slideVal
                slideVal = slideIndex - slidesRange
                data = booksDataClone.slice(slideVal - slidesRange, slideVal)
                setSlidesData(data)
                prevSlideFunc(slideIndex, slidesRange)
            } else {
                let data
                let remainder_val = slideIndex % slidesRange
                let slideVal = (slideIndex - remainder_val)
                data = booksDataClone.slice(slideVal - 6, slideVal)
                setSlidesData(data)
                prevSlideFunc(slideIndex, remainder_val)
            }
        } else {
            if (slideIndex <= 6) {
                let startSlide = 0;
                let endSlide = slideIndex
                let data = booksDataClone.slice(startSlide, endSlide)
                setSlidesData(data)
                setSlideIndex(endSlide)
            }
        }
    }

    const prevSlideFunc = (index, val) => {
        setSlideIndex(index - val)
    }

    const goToNextSlide = () => {
        let booksDataClone = booksData.slice()
        let remaining_len = (booksDataClone.length - slideIndex)
        if (remaining_len > 0) {
            if (slideIndex === remaining_len) {
                return
            }
            if (remaining_len >= 6) {
                let data = booksDataClone.slice(slideIndex, (slideIndex + 6))
                setSlidesData(data)
                nextSlideFunc(slideIndex, 6)
            } else {
                let data = booksDataClone.slice(slideIndex, (slideIndex + remaining_len))
                setSlidesData(data)
                nextSlideFunc(slideIndex, remaining_len)
            }
        }
    }

    const nextSlideFunc = (index, val) => {
        setSlideIndex(index + val);
    }

    let addToCart = (item) => {
        onAddToCart(item)
    }

    const dragStart = (e, position) => {
        dragItem.current = position;
        console.log(e.target.innerHTML);
    };

    return (
        <>
            {CarKey ?
                <div className="row">
                    <div className="col-lg-1">
                        {(slideIndex === 6) ?
                            ''
                            :
                            <BackArrow className="" onClick={goToPrevSlide}>
                                <i className='fa fa-angle-left fa-3x ' aria-hidden='true'></i>
                            </BackArrow>
                        }
                    </div>
                    <div className="col-lg-10">
                        <div style={{ display: 'flex', marginLeft: '20px', marginRight: '20px' }}>
                            {slidesData.map((item, i) =>

                            (<Card key={i}
                                >
                                {item ?
                                    <>
                                        <Container>

                                            <Link to={{ pathname: `/detail/${item._id}/${item.key}`, detailsData: item }}>
                                                <img alt="" src={avatar} style={{ width: '100%', height: "50%", cursor: 'pointer' }} />
                                            </Link>
                                            <ElemProps>
                                                <div>
                                                    {item ? item.title : null}
                                                </div>
                                                <p>{item ? item.author : null}</p>
                                                <p>{item ? item.price : null}</p>
                                            </ElemProps>
                                            <button style={{ backgroundColor: 'blue', color: 'white', textAlign: "center", display: "block", margin: '0 auto' }} onClick={() => addToCart(item)}>Quick Add</button>

                                        </Container>

                                    </>
                                    : null}
                            </Card>)
                            )}
                        </div>
                    </div>
                    <div className="col-lg-1">
                        {slideIndex === booksJson.length ?
                            ''
                            :
                            <BackArrow onClick={goToNextSlide}>
                                <i className='fa fa-angle-right fa-3x ' aria-hidden='true'></i>
                            </BackArrow>
                        }
                    </div>
                </div >
                :
                <p>loading</p>
            }
        </>

    )
}
export default CarouselComponent;

const Card = styled.div`
box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
transition: 0.3s;
width: 40%;
&:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
  }
`;

const Container = styled.div`
padding: 2px 16px;
cursor:pointer;
height: 350px;
`;

const ElemProps = styled.div`
height: 100px;
`

const BackArrow = styled.div`
border-radius: 50%;
    background-color: #04AA6D;
    border: none;
    color: white;
    padding: 12px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 11px;
    margin: 4px 2px;
    cursor: pointer;
    margin-top: 110px;
`;



