import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { omit } from 'lodash'
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import "./Register.css";
import { GET_ERRORS } from '../actions/types';

const Register = (props) => {
    const navigate = useNavigate();
    let dispatch = useDispatch();
    const params = useParams();
    let [user, setUser] = useState({
        name: '',
        email: '',
        password: '',
        password_confirm: '',
    });
    const [successMessage, setSuccessMessage] = useState(null)
    const [showError, setShowError] = useState(null)
    const [errors, setErrors] = useState({})
    const [fieldValidationErrors, setFieldValidationErrors] = useState({})


    const handleInputChange = (e) => {
        const { name, value } = e.target;
        validateForm(e, name, value)
        setUser(user => ({ ...user, [name]: value }));
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        // validateForm()
        // let { name, email, password, password_confirm } = user
        // if (name && email && password && password_confirm) {
        registerUser(user);
        // }
    }

    let validateForm = (event, name, value) => {
        // let { name, email, password, password_confirm } = user;
        let regex = '/^(([^<>()[\]\\.,;:\s@"]+(\.[^ <>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1, 3}\.[0-9]{1, 3}\.[0-9]{1, 3}\.[0-9]{1, 3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/</>';
        let passwordRegex = '/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/';
        switch (name) {
            case 'name':
                if (value.length <= 4) {
                    // we will set the error state

                    setErrors({
                        ...errors,
                        username: 'Username atleast have 5 letters'
                    })
                } else {
                    // set the error state empty or remove the error for username input

                    //omit function removes/omits the value from given object and returns a new object
                    let newObj = omit(errors, "name");
                    setErrors(newObj);

                }
                break;
            case "email":
                if (!new RegExp(regex).test(value)) {
                    setErrors({
                        ...errors,
                        email: 'Enter a valid email address'
                    })
                } else {
                    let newObj = omit(errors, "email");
                    setErrors(newObj);
                }
                break;
            case "password":
                if (!new RegExp(passwordRegex).test(value)) {
                    setErrors({
                        ...errors,
                        password: 'Password should contains atleast 8 charaters and containing uppercase,lowercase and numbers'
                    })
                } else {
                    let newObj = omit(errors, "password");
                    setErrors(newObj);
                }
            default:
                break;
        }
    }

    let registerUser = (state) => {
        if (state.email.length && state.password.length) {
            setShowError(null);

            axios.post('/api/users/register', state)
                .then(response => {
                    if (response?.data?.code === 200)
                        setSuccessMessage("Registration successful. Redirecting to login page..")
                    else
                        setShowError("Some error ocurred");
                    setShowError(null)
                    navigate('/login')
                })
                .catch(err => {
                    console.log(err);
                    dispatch({
                        type: GET_ERRORS,
                        payload: err.response.data
                    });
                });
        } else {
            setShowError('Please enter valid username and password')
        }
    }

    const redirectToLogin = () => {
        // props.updateTitle('Login')
        navigate('/login');
    }

    return (
        <>
            {/* <h1>Register screen</h1> */}
            <div className="container" style={{ marginTop: '50px', width: '700px' }}>
                <h2 style={{ marginBottom: '40px' }}>Registration</h2>
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <input
                            type="text"
                            placeholder="Name"
                            className="form-control"
                            name="name"
                            onChange={handleInputChange}
                            value={user.name}
                        />
                        {errors.name && (<div className="invalid-feedback">{errors.name}</div>)}
                    </div>
                    <div className="form-group">
                        <input
                            type="email"
                            placeholder="Email"
                            className="form-control"
                            name="email"
                            onChange={handleInputChange}
                            value={user.email}
                        />
                        {errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
                    </div>
                    <div className="form-group">
                        <input
                            type="password"
                            placeholder="Password"
                            className="form-control"
                            name="password"
                            onChange={handleInputChange}
                            value={user.password}
                        />
                        {errors.password && (<div className="invalid-feedback">{errors.password}</div>)}
                    </div>
                    <div className="form-group">
                        <input
                            type="password"
                            placeholder="Confirm Password"
                            className="form-control"
                            name="password_confirm"
                            onChange={handleInputChange}
                            value={user.password_confirm}
                        />
                        {errors.password_confirm && (<div className="invalid-feedback">{errors.password_confirm}</div>)}
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary">
                            Register User
                        </button>
                    </div>
                </form>
                <div className="alert alert-success mt-2" style={{ display: successMessage ? 'block' : 'none' }} role="alert">
                    {successMessage}
                </div>
                <div className="mt-2">
                    <span>Already have an account? </span>
                    <span className="loginText" onClick={() => redirectToLogin}>Login here</span>
                </div>
            </div>
        </>
    )
}

export { Register }
