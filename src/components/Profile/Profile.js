import React, { useState, useEffect, useCallback, } from "react";
import axios from 'axios';
import { Buffer } from 'buffer';
import { useDispatch } from "react-redux";

import { imageUpload } from "../../actions/uploadAction";
import Navbar from "../Navbar";
import { setProfileImageMethod } from "../../actions/profileImageAction"
import profile_image from "../../assets/default_user_image.jpg"

export const ProfileComponent = () => {
    const [selectedFiles, setSelectedFiles] = useState(null);
    const [currentFile, setCurrentFile] = useState(null);
    const [progress, setProgress] = useState(0);
    const [message, setMessage] = useState("");
    const [fileInfos, setFileInfos] = useState([]);
    const [profileImage, setProfileImage] = useState(null);
    let dispatch = useDispatch();

    let getProfileImage = useCallback(
        async () => {
            await axios.get('/api/books/get-profile-image', { responseType: 'blob' })
                .then(res => {
                    console.log("res.data:", res.data)
                    console.log("image details from Profile:", res.data);
                    setProfileImage(res.data);
                    dispatch(setProfileImageMethod(res.data));
                })
                .catch(err => {
                    console.log("err:", err)
                });
        }, [profileImage])

    useEffect(() => {
        getProfileImage()
    }, []);

    const selectFile = (event) => {
        event.preventDefault()
        setSelectedFiles(event.target.files);
    };

    const upload = async () => {
        if (selectedFiles.length > 0) {
            let currentFile = selectedFiles?.[0];

            setProgress(0);
            setCurrentFile(currentFile);

            await imageUpload(currentFile, (event) => {
                setProgress(Math.round((100 * event.loaded) / event.total));
            })
                .then((response) => {
                    setMessage(response?.data?.message);
                    getProfileImage()
                })
                .catch((err) => {
                    setProgress(0);
                    setMessage(err);
                    setCurrentFile(null);
                });
            setSelectedFiles(null);
            // setMessage(null)
            setCurrentFile(null)
            setProgress(0);
            setTimeout(() => {
                setMessage(null)
            }, 5000);
        }
    };

    let callNavBar = () => {
        return (
            <>
                <Welcome info="helloo from profile" />
            </>
        )
    }

    const Welcome = ({ info }) => {
        return <h1>{info}</h1>;
    };

    return (
        <div>
            <h3>Profile Picture</h3>
            {currentFile ? (
                <div className="progress">
                    <div
                        className="progress-bar progress-bar-info progress-bar-striped"
                        role="progressbar"
                        aria-valuenow={progress}
                        aria-valuemin="0"
                        aria-valuemax="100"
                        style={{ width: progress + "%" }}
                    >
                        {progress}%
                    </div>
                </div>
            ) : null}

            <label className="btn btn-default">
                <input type="file" onChange={selectFile} />
            </label>

            <button
                className="btn btn-success"
                // disabled={!selectedFiles}
                onClick={upload}
            >
                Upload
            </button>

            <div className="alert alert-light" role="alert">
                {message}
            </div>
            {profileImage != null && <img src={profileImage ? URL.createObjectURL(profileImage) : null} width="300"
                alt="image1" />}
        </div>
    );
}