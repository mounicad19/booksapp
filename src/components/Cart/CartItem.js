import React, { useState, useEffect } from "react";
import "../.././styles/scss/styles.scss";
import "./Cart.css";
import novel_img from "../../assets/Redwall.jpg"

import { getAllCartItems, updateCartquantity, removeProductFromCart } from "../../actions/cartAction";

export const CartItem = ({ cartItems, onUpdateCartQty, onRemoveFromCart }) => {
    const [cart, setCart] = useState([]);
    const [finalPrice, setFinaPrice] = useState(0)

    useEffect(() => {
        let getCartItems = async () => {
            let cartData = await getAllCartItems();
            setCart(cartData.data)
        }
        getCartItems();
        calculateFinalPrice()
    }, [cart, finalPrice])

    const handleUpdateCartQty = async (id, op) => {
        let updatedVal = await updateCartquantity(id, op);
        setCart(prevCart =>
            prevCart.map((item, i) => {
                if (id === item[id]) {
                    return { ...updatedVal.data, item };
                }
                return item;
            })
        )
    }

    const handleRemoveFromCart = async (id) => {
        let response = await removeProductFromCart(id);
        let resultedItems = cart.filter((item) => item.id != id)
        setCart(resultedItems)
    }

    const calculateFinalPrice = () => {
        let initialValue = 0;
        const total = cart.reduce((accumulator, current) => accumulator + current.price * current.quantity, initialValue)
        setFinaPrice(total)
    }

    const checkoutFn=()=>{

    }

    return (

        <div className="cart-item cart_main_div">
            <div className="shopping_cart">
                <h4 className="cart__heading">My Shopping Cart</h4>

                <div className="cart_products_div">
                    {cart.map((item, i) => <>
                        <div className="cart_item_details" key={i}>
                            <div className="row" style={{ marginBottom: '1rem' }}>

                                <span className="product_title">{item.title}</span>
                                <span className="product_author">{item.author}</span>

                                <div className="col-lg-3">
                                    <img src={novel_img} className="book_image_div" />
                                </div>
                                <div className="col-lg-3">

                                    <div className="cart_quantity">
                                        <button type="button" onClick={() => item.quantity > 1 ? handleUpdateCartQty(item._id, "decrement") : handleRemoveFromCart(item._id)}>-</button>
                                        <p>{item.quantity}</p>
                                        <button type="button" onClick={() => handleUpdateCartQty(item._id, "increment")}>+</button>
                                    </div>
                                    <div className="cart-item__details-price">${item.price}</div>
                                    <button
                                        type="button"
                                        className="cart-item__remove"
                                        onClick={() => handleRemoveFromCart(item._id)}
                                    >
                                        Remove
                                    </button>
                                </div>
                            </div>

                        </div>

                        <span style={{ borderBottom: '1px solid #ccc' }} />

                    </>)}
                </div>
            </div>
            <div className="checkout_div">
                <h4 className="cart__heading">Order summary</h4>
                <p>Subtotal({cart.length} items) : ${finalPrice}</p>
                <br></br>
                <p>Order Total: ${finalPrice}</p>
                <button style={{background: "#a9a9f5", border: "1px solid blue"}} onClick={checkoutFn}>Checkout</button>
            </div>
        </div>
    );


}
