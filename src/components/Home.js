import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee, faSpinner } from '@fortawesome/free-solid-svg-icons'
import CarouselComponent from './Carousel';
import { getAllCartItems, addToCart } from "./../actions/cartAction";


const Home = (props) => {
    const [generalBooks, setGeneralBooks] = React.useState([])
    const [cartItems, setCartItems] = useState([]);
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        getBooks()
    }, [])

    useEffect(() => {
        let getCartItems = async () => {
            let cartData = await getAllCartItems();
            setCartItems(cartData.data)
        }
        getCartItems();
    }, [cartItems])

    const getBooks = () => {
        axios.get('/api/books/get-books')
            .then(res => {
                setTimeout(() => {
                    setGeneralBooks(res.data)
                    setLoading(false)
                }, 2000)
            }).catch(err => {
                console.log("err:", err)
            })
    }

    let onAddToCart = async (item) => {
        let requiredObj = {
            title: item.title,
            author: item.author,
            price: "20",
            quantity: item.quantity,
            createdAt: item.createdAt,
            updatedAt: item.updatedAt
        }
        await addToCart(requiredObj)
    }

    return (
        <>
            <h1>Home screen</h1>
            <div>
                <div>
                    Bestsellers
                </div>
                <div>This year's top sellers</div>
                {generalBooks.length > 0 ?
                    <CarouselComponent carkey={false} booksProps={generalBooks}
                        onAddToCart={onAddToCart}
                    />
                    : <>{loading ? (<div style={{
                        display: "block",
                        marginLeft: "auto",
                        marginRight: "auto",
                        width: 50
                    }}><FontAwesomeIcon icon={faSpinner} pulse /></div>)
                        : <h2 style={{textAlign:"center"}}>No books to display</h2>}</>
                }
            </div>
        </>
    )
}
export default Home