import React, { useState, useReducer } from 'react';
import { useNavigate } from 'react-router-dom';
// import { loginUser } from '../actions/authentication';
import { useDispatch, useSelector } from "react-redux";
import jwt_decode from 'jwt-decode';
import axios from "axios"

import setAuthToken from '../setAuthToken';
import { GET_ERRORS, SET_CURRENT_USER } from '../actions/types';
import auth from '../reducers/authReducer';
import {authReducer} from '../reducers/authReducer';

const Login = (props) => {
    const navigate = useNavigate()
    let [userForm, setUserForm] = useState({
        email: '',
        password: ''
    });
    // const [errors, setErrors] = useState({})
    // let  authValue  = useSelector(state => state.auth);
    // let { errors } = useSelector(state => state.errors)

    let [state, dispatch]= useReducer(authReducer, auth.initialState)



    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setUserForm(userForm => ({ ...userForm, [name]: value }));
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (userForm.email && userForm.password) {
            loginUser(userForm);
            // navigate('/');
        }
    }

    let loginUser = (user) => {
        axios.post('/api/users/login', user)
            .then(res => {
                const { token } = res.data;
                localStorage.setItem('jwtToken', token);
                setAuthToken(token);
                const decoded = jwt_decode(token);
                dispatch(setCurrentUser(decoded));
                navigate("/")
            })
            .catch(err => {
                dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            });
    }

    let setCurrentUser = (decoded) => {
        return {
            type: SET_CURRENT_USER,
            payload: decoded
        }
    }

    return (
        <>
            <h1>Login screen</h1>
            <div className="container" style={{ marginTop: '50px', width: '700px' }}>
                <h2 style={{ marginBottom: '40px' }}>Login</h2>
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <input
                            type="email"
                            placeholder="Email"
                            className="form-control"
                            name="email"
                            onChange={handleInputChange}
                            value={userForm.email}
                        />
                        {/* {errors.email && (<div className="invalid-feedback">{errors.email}</div>)} */}
                    </div>
                    <div className="form-group">
                        <input
                            type="password"
                            placeholder="Password"
                            className="form-control"
                            name="password"
                            onChange={handleInputChange}
                            value={userForm.password}
                        />
                        {/* {errors.password && (<div className="invalid-feedback">{errors.password}</div>)} */}
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary">
                            Login User
                        </button>
                    </div>
                </form>
            </div>
        </>
    )
}


export default Login