import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import jwt_decode from 'jwt-decode';
import { PersistGate } from 'redux-persist/integration/react'

import setAuthToken from './setAuthToken';
// import { setCurrentUser, logoutUser } from './actions/authentication';
import { store, persistor } from './store';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/scss/styles.scss'
import RoutesApp from './Routes/Routes';

let setCurrentUser = decoded => {
    return {
        type: "SET_CURRENT_USER",
        payload: decoded
    }
}

// let logoutUser = () => {
//     // let dispatch = useDispatch();
//     // const navigate = useNavigate();
//     localStorage.removeItem('jwtToken');
//     setAuthToken(false);
//     store.dispatch(setCurrentUser({}));
//     navigate('/login');
// }

if (localStorage.jwtToken) {
  setAuthToken(localStorage.jwtToken);
  const decoded = jwt_decode(localStorage.jwtToken);
  store.dispatch(setCurrentUser(decoded));
  const currentTime = Date.now() / 1000;
  // if (decoded.exp < currentTime) {
  //   store.dispatch(logoutUser());
  //   window.location.href = '/login'
  // }
}

function App() {
  return (
    <Provider store={store}>
      {/* <PersistGate loading={null} persistor={persistor}> */}
        <RoutesApp />
      {/* </PersistGate> */}
    </Provider>
  );
}

export default App;
