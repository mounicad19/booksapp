import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Login from '../components/Login'
import Home from '../components/Home';
import { Register } from '../components/Register';
import BookDetail from '../components/BookDetail';
import Navbar from '../components/Navbar';
import { CartItem } from "../components/Cart/CartItem";
import { ProfileComponent } from '../components/Profile/Profile'
import 'bootstrap/dist/css/bootstrap.min.css';

function RoutesApp() {
    return (
        <Router>
            <div>
                <Navbar />
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/register" element={<Register />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/detail/:id/:key" element={<BookDetail />} />
                    <Route path="/cart" element={<CartItem />} />
                    <Route path="/profile" element={<ProfileComponent />} />
                </Routes>
            </div>
        </Router>
    )
}

export default RoutesApp