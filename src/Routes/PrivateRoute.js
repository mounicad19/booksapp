// import React from 'react';
// import { Route, Redirect } from 'react-router-dom';
// import setAuthToken from '../setAuthToken'
// import jwt_decode from 'jwt-decode';
// import store from '../store';

// import { setCurrentUser, logoutUser } from '../actions/authentication';

// const PrivateRoute = ({ component:Component, roles, ...rest }) => {
//     return (
//         <Route {...rest} render={props => {
//             if (!localStorage.jwtToken) {
//                 // not logged in so redirect to login page with the return url
//                 return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
//             }

//             // logged in so return component
//             return <Component {...props} />
//         }} />
//     )
// }

// export default PrivateRoute