// store.js

import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const inititalState = {};
const persistConfig = {
    key: 'root',
    storage,
}
// const persistedReducer = persistReducer(persistConfig, rootReducer)
export const store = createStore(
    // persistedReducer,
    rootReducer,
    inititalState,
    applyMiddleware(thunk)
)
// export const persistor = persistStore(store)
