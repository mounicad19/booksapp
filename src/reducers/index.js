import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import { combineReducers } from 'redux';
import errorReducer from './errorReducer';
import {authReducer} from './authReducer';
import { bookDetailsReducer } from './bookDetailReducer';
import {imageReducer} from "./updateImageReducer"

export default combineReducers({
    details: bookDetailsReducer,
    errors: errorReducer,
    auth: authReducer,
    imageDetails: imageReducer
});