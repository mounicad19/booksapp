import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const initialState = {
    details: {},
    booksArr:[]
}

// const detailsPersistConfig = {
//     key: 'details',
//     storage: storage,
//     whitelist: ['details'],
// }

export const bookDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_BOOK_DETAILS":
            return {
                ...state,
                // localStorage.setItem('formData', JSON.stringify(action.payload),
                details: action.payload
            }
        case "GET_BOOK_DETAILS":
            return {
                ...state,
                details: action.payload,
            }
        case "GET_BOOKS":
            return {
                ...state,
                booksArr: action.payload
            }
        default:
            return state;
    }
}
