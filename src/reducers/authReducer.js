import { SET_CURRENT_USER } from '../actions/types';
// import isEmpty from '../validation/is-empty';

const initialState = {
    isAuthenticated: false,
    user: {}
}

export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CURRENT_USER:
            return {
                ...state,
                isAuthenticated: !isEmpty(action.payload),
                user: action.payload
            }
        default:
            return state;
    }
}

const isEmpty = (val) => {
    switch (val) {
        case undefined:
            return true
        case null:
            return true
        case (typeof val === 'object' && Object.keys(val).length === 0):
            return true
        case (typeof val === 'string' && val.trim().length === 0):
            return true
        default:
    }
}

export default initialState