import { SET_PROFILE_IMAGE } from '../actions/types';

const initialState = {
    imagePayload: {}
}

export const imageReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_PROFILE_IMAGE:
            return {
                ...state,
                img: action.payload
            }
        default:
            return state;
    }
}