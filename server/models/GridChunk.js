const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GridChunkSchema = new Schema({
    files_id: {
        type: Schema.Types.ObjectId,
        ref: 'GridFile'
    },
    n: {
        type: Number
    },
    data: {
        type: Buffer
    }
}, { timestamps: true });

const GridChunk = mongoose.model('GridChunk', GridChunkSchema);
module.exports = GridChunk;
