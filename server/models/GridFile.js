//Grid.File

import mongoose from "mongoose";
const Schema = mongoose.Schema;

const GridFileSchema = new Schema({
    filename: {
        type: String,
        required: true
    },
    length: {
        type: Number
    },
    chunkSize: {
        type: Number
    },
    uploadDate: {
        type: Date
    },
    md5: {
        type: String,
        default: ''
    },
    contentType:{
        type: String,
    },
    aliases:{
        type: [String],
    },
    metaData:{
        type: String,
    }
},
    { timestamps: true }
);

export const GridFile = mongoose.model('GridFile', GridFileSchema);