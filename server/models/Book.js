//Books.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const File = require('./File')

const BookSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    author: {
        type: String,
    },
    language: {
        type: String,
    },
    file: {
        type: Schema.Types.ObjectId, ref: 'File'
    },
    pages: {
        type: Number,
    },
    year: {
        type: Number
    },
    key: {
        type: String
    },
    country: {
        type: String
    },
    price: {
        type: Number
    },
    imageUrl: String
    
}, { timestamps: true });

const Book = mongoose.model('Book', BookSchema);

module.exports = Book;