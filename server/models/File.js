//Books.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FileSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  file: {
    type: Buffer,
    required: true
  }
},
  { timestamps: true }
);

const File = mongoose.model('File', FileSchema);
module.exports = File;