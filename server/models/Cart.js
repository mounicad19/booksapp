//Books.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const File = require('./File')

const CartSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    author: {
        type: String,
    },
    price: {
        type: Number,
    },
    quantity: {
        type: Number,
        default: 1
    }
}, { timestamps: true });

const Cart = mongoose.model('Cart', CartSchema);

module.exports = Cart;