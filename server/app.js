const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');
const cors = require('cors');
var path = require("path");
// var multer = require('multer');
// const Grid = require('gridfs-stream');
// const GridFsStorage = require('multer-gridfs-storage').GridFsStorage;
const { MongoClient } = require('mongodb');

const config = require('./db');
const users = require('./routes/userRoute');
const booksRoute = require("./routes/booksRoute");
const cartRoute = require('./routes/cartRoute')
// const saveBooksJobFile = require("./jobs/booksJob")

// const client = new MongoClient(config.DB);

let dbConnection = mongoose.connect(config.DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log('Database is connected');
}).catch(err => {
    console.error('Can not connect to the database', err);
    // process.exit(1)
});

const app = express();
app.use(passport.initialize());
require('./passport')(passport);
app.use(cors({ origin: true }));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static(path.join(__dirname, "public")));
console.log("__dirname: ", __dirname);
app.get('/', function (req, res) {
    console.log("__dirname: ", __dirname);
    res.send('hello');
});

// Create storage engine

// app.post('/upload', upload.single('file'), (req, res) => {
//     res.redirect('/');
// });

// ===========>
// Pass storage value as param to books route, and access the above function from there
// Pass the image data as input to route to uplioad function


app.use('/api/users', users);
app.use('/api/books', booksRoute)
app.use('/api/cart', cartRoute)


const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Server is running on PORT ${PORT}`);
});

// let booksVar = saveBooksJobFile.saveBooksJob()

module.exports = app;