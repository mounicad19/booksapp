
const express = require('express');
const cartRouter = express.Router();
const CartModel = require('../models/Cart');


exports.deleteCart = async (id) => {
    return await CartModel.findByIdAndDelete(id);
};

exports.decrementCart = async(id)=>{
    return await CartModel.findByIdAndUpdate(id, {$inc: {quantity: -1}});    
}

exports.incrementCart = async(id)=>{
    return await CartModel.findByIdAndUpdate(id, {$inc: {quantity: 1}});    
}