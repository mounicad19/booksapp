
const Book = require('../models/Book');
const FileDb = require("../models/File");
// const pdfparse = require('pdf-parse');
const pdfParser = require("pdf2json");
var fs = require('fs');
// const booksJson = require("../json_files/books.js")
// const sampleBook = require("../pdf_files/final.pdf")

// const saveBook = () => {
//   console.log("save books")
//   let newBooks = new Book({})
//   booksJson.generalBooks.map((book) => {
//     newBooks.title = title;
//     newBooks.author = author;
//     newBooks.country = country
//     newBooks.language = language;
//     newBooks.pages = pages
//     newBooks.year = year;
//     newBooks.price = price
//     newBooks.key = key
//     if (book.file) {
//       returnedFile = savePdfFile(book.file)
//     }
//     newBooks.file = returnedFile.id;
//     let saveResp = newBooks.save();
//     console.log("saveResp:", saveResp)
//   })
// }

const savePdfFile = async (data) => {
  try {
    let fileVar = new FileDb({
      file: data.text
    });
    let fileResp = await fileVar.save();
    console.log("fileResp:", fileResp);
  } catch (err) {
    console.log("err:", err)
  }
}

const retrieveBooks = async () => {
  await Book.find((err, resp) => {
    console.log("books fetch resp:", resp);
  });
}

module.exports = {
  // saveBook,
  retrieveBooks,
  savePdfFile
}

