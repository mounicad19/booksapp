
const config = require('../db');
// const Agenda = require('agenda');
const cron = require('node-cron');
const books = require("./saveBooks.js");
const pdfParse = require('pdf-parse');
var fs = require('fs');
const FileDb = require("../models/File");

// var PdfReader = require('pdfreader').PdfReader;
// const generalBooks = require("../json_files/generalBooks")

// cron.schedule("* * * * *", () => {
//   console.log("job running every 1 minute");
// })

const saveBooksJob = cron.schedule("* * * * *", async () => {
  // return new Promise(async (resolve, reject) => {

  let filePath = './sample2.pdf'

  const buffer = fs.readFileSync(filePath);
  try {
    const data = await pdfParse(buffer);

    // The content
    console.log('Content: ', data.text);

    // Total page
    console.log('Total pages: ', data.numpages);

    // File information
    console.log('Info: ', data.info);
    savePdfFile(data)
  } catch (err) {
    console.log("err:", err)
  }
});

const savePdfFile = async (data) => {
  try {
    let fileVar = new FileDb({
      file: data.text
    });
    let fileResp = await fileVar.save();
    console.log("fileResp:", fileResp);
  } catch (err) {
    console.log("err:", err)
  }
}

module.exports.saveBooksJob = saveBooksJob
 
