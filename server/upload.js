
const util = require("util");
const multer = require("multer");
const {GridFsStorage} = require("multer-gridfs-storage")
const dbConfig = require("./db");

var storage = new GridFsStorage({
  url: dbConfig.DB,
  options: { useNewUrlParser: true, useUnifiedTopology: true },
  file: (req, file) => {
    const match = ["image/png", "image/jpeg", "image/jpg", "application/pdf"];
    if (match.indexOf(file.mimetype) === -1) {
      const filename = `${Date.now()}-bezkoder-${file.originalname}`;
      return filename;
    }
    return {
      bucketName: dbConfig.bucketName,
      filename: `${Date.now()}-bezkoder-${file.originalname}`
    };
  }
});

var uploadFiles = multer({ storage }).array("file", 10);
module.exports = uploadFiles



