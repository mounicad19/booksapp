

const {
    a_christmas_carol,
    pride_and_prejudice,
    pdf_1984,
    salems_lot,
    tender_is_the_night,
    The_Adventures_of_Sherlock_Holmes,
    The_Divine_Comedy,
    Wuthering_Heights,
    ficciones_novel,
    the_epic_of_gilgamesh_ebook,
    book_of_job
} = require('../public/files')

var {
    a_christmas_carol_img,
    fairy_tales,
    Pride_Prejudice,
    the_epic_of_gilgamesh,
    the_book_of_job,
    the_divine_comedy,
    wuthering_heights,
    One_Thousand_and_One_Nights,
    Salems_lot,
    ficciones,
    adventures_of_sherlock_holmes
} = require('../public/images')

const generalBooks = [
    {
        "author": "Chinua Achebe",
        "country": "Nigeria",
        "imageLink": a_christmas_carol_img,
        "language": "English",
        "file": a_christmas_carol,
        "pages": 209,
        "title": "A Christmas Carol",
        "year": 1958,
        "price": 4,
        "key": "general"
    },
    {
        "author": "Hans Christian Andersen",
        "country": "Denmark",
        "imageLink": fairy_tales,
        "language": "Danish",
        "file": pdf_1984,
        "pages": 784,
        "title": "Fairy tales",
        "year": 1836,
        "price": 4,
        "key": "general"
    },
    {
        "author": "Dante Alighieri",
        "country": "Italy",
        "imageLink": the_divine_comedy,
        "language": "Italian",
        "file": The_Divine_Comedy,
        "pages": 928,
        "title": "The Divine Comedy",
        "year": 1315,
        "price": 6,
        "key": "general"
    },
    {
        "author": "Unknown",
        "country": "Sumer and Akkadian Empire",
        "imageLink": the_epic_of_gilgamesh,
        "language": "Akkadian",
        "file": the_epic_of_gilgamesh_ebook,
        "pages": 160,
        "title": "The Epic Of Gilgamesh",
        "year": -1700,
        "price": 6,
        "key": "general"
    },
    {
        "author": "Unknown",
        "country": "Achaemenid Empire",
        "imageLink": the_book_of_job,
        "language": "Hebrew",
        "file": book_of_job,
        "pages": 176,
        "title": "The Book Of Job",
        "year": -600,
        "price": 8,
        "key": "general"
    },
    {
        "author": "Stephen King",
        "country": "Iceland",
        "imageLink": Salems_lot,
        "language": "Old Norse",
        "file": salems_lot,
        "pages": 384,
        "title": "Salem's Lot",
        "year": 1350,
        "price": 9,
        "key": "general"
    },
    {
        "author": "Jane Austen",
        "country": "United Kingdom",
        "imageLink": Pride_Prejudice,
        "language": "English",
        "file": pride_and_prejudice,
        "pages": 226,
        "title": "Pride and Prejudice",
        "year": 1813,
        "price": 3,
        "key": "general"
    },
    {
        "author": "Jorge Luis Borges",
        "country": "Argentina",
        "imageLink": ficciones,
        "language": "Spanish",
        "file": ficciones_novel,
        "pages": 224,
        "title": "Ficciones",
        "year": 1965,
        "price": 9,
        "key": "general"
    },
    {
        "author": "Emily Bront",
        "country": "United Kingdom",
        "imageLink": wuthering_heights,
        "language": "English",
        "file": Wuthering_Heights,
        "pages": 342,
        "title": "Wuthering Heights",
        "year": 1847,
        "price": 3,
        "key": "general"
    },
    {
        "author": "Arthur Conan Doyle",
        "country": "United Kingdom",
        "imageLink": adventures_of_sherlock_holmes,
        "language": "English",
        "file": The_Adventures_of_Sherlock_Holmes,
        "pages": 226,
        "title": "The Adventures of Sherlock Holmes",
        "year": 1880,
        "price": 11,
        "key": "mystery"
    }
]

module.exports = {generalBooks}