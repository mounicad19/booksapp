let path = require("path");
let a_christmas_carol_img = path.join("public/images", "A_Christmas_Carol.jpg");
let fairy_tales = path.join("public/images", "fairy_tales.jpeg");
let Pride_Prejudice = path.join("public/images", "Pride-Prejudice.jpg");
let the_epic_of_gilgamesh = path.join("public/images", "the_epic_of_gilgamesh.jpg");
let the_book_of_job = path.join("public/images", "the-book-of-job.jpg");
let the_divine_comedy = path.join("public/images", "the-divine-comedy-51.jpg");
let Things_Fall_Apart = path.join("public/images", "Things-Fall-Apart.png");
let wuthering_heights = path.join("public/images", "wuthering_heights.jpeg");
let One_Thousand_and_One_Nights = path.join("public/images", "One Thousand and One Nights.jpeg");
let salems_lot_img = path.join("public/images", "salems lot.jpg");
let ficciones = path.join("public/images", "ficciones.jpg");
let adventures_of_sherlock_holmes = path.join("public/images", "adventures_of_sherlock_holmes.jpg");

module.exports = {
    a_christmas_carol_img,
    fairy_tales,
    Pride_Prejudice,
    the_epic_of_gilgamesh,
    the_book_of_job,
    the_divine_comedy,
    Things_Fall_Apart,
    wuthering_heights,
    One_Thousand_and_One_Nights,
    salems_lot_img,
    ficciones,
    adventures_of_sherlock_holmes
}