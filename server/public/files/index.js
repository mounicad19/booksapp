let path = require("path");
let a_christmas_carol = path.join("public/files", "a_christmas_carol.pdf");
let pride_and_prejudice = path.join("public/files", "pride_and_prejudice.pdf");
let pdf_1984 = path.join("public/files", "1984.pdf");
let salems_lot = path.join("public/files", "salems-lot.pdf");
let tender_is_the_night = path.join("public/files", "tender_is_the_night.pdf");
let The_Adventures_of_Sherlock_Holmes = path.join("public/files", "The_Adventures_of_Sherlock_Holmes.pdf");
let The_Divine_Comedy = path.join("public/files", "Dante-Alighieri-The-Divine-Comedy.pdf");
let Wuthering_Heights = path.join("public/files", "Wuthering Heights.pdf");
let ficciones_novel = path.join("public/files", "ficciones.pdf");
let the_epic_of_gilgamesh_ebook = path.join("public/files", "eog.pdf");
let book_of_job = path.join("public/files", "book_of_job.pdf");

module.exports = {
    a_christmas_carol,
    pride_and_prejudice,
    pdf_1984,
    salems_lot,
    tender_is_the_night,
    The_Adventures_of_Sherlock_Holmes,
    The_Divine_Comedy,
    Wuthering_Heights,
    ficciones_novel,
    the_epic_of_gilgamesh_ebook,
    book_of_job
}