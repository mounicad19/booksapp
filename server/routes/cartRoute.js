// cart.js

const express = require('express');
const cartRouter = express.Router();

const Cart = require('../models/Cart');
const CartService = require('../services/cartService')

let findAndUpdateFunc = async (obj, res) => {
    // let { title, quantity } = obj
    // let quantityVal = flag != null && flag ? (res?.quantity+flag) : (res?.flag+1)
    try {
        let queryVal = await Cart.findOneAndUpdate(
            {
                id: obj.id
            },
            {
                $inc: {
                    quantity: obj.quantity
                }
            },
            { useFindAndModify: false }
        )
        res.json({ data: queryVal, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

const saveCartItems = async (body, res) => {
    let { title, author, price, quantity } = body
    const newItem = new Cart({
        title,
        author,
        price,
        quantity
    });
    await newItem
        .save()
        .then(result => {
            // return ({ response: result, status: 200 })
            return res.status(200).json(result)
        }).catch(err => {
            console.log("err saving product to cart:", err)
            return res.status(400).json(err)
        })
}

cartRouter.post('/add', async (req, res) => {
    // let { items } = req.body
    let { quantity, title, author, price } = req.body;
    let flag = quantity;

    await Cart.findOne({
        title: req.body.title
    }, async (err, resp) => {
        if (err) {
            return res.status(400).json(err)
        } else {
            if (resp && resp._id) {
                try {
                    let cartResponse = await CartService.incrementCart(resp._id);
                    return res.json({ data: cartResponse, status: "success" });
                } catch (err) {
                    // return res.json({ data: cartResponse, status: "success" });
                    res.status(500).json({ error: err.message });
                }
            } else {
                await saveCartItems(req.body, res)
            }
        }
    })
});

cartRouter.get('/get-all', async (req, res) => {
    await Cart.find({}).then(response => {
        return res.status(200).json(response)
    }).catch(err => {
        console.log("err fetching Cart products :", err)
        res.status(400).json(err)
    })
})

cartRouter.get('/delete/:id', async (req, res) => {
    let { id } = req.params;
    try {
        const cartData = await CartService.deleteCart(id);
        res.json({ data: cartData, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});

cartRouter.patch('/updateQuantity/:op/:id', async (request, response) => {
    const { id, op } = request.params;
    try {
        let cartResponse

        if (op == "increment") {
            cartResponse = await CartService.incrementCart(id);
        } else {
            cartResponse = await CartService.decrementCart(id);
        }
        if (cartResponse) {
            response.json({ data: cartResponse, status: "success" });
        }
    } catch (error) {
        response.status(500).json({ error: err.message });
    }
})


module.exports = cartRouter;