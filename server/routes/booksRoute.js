const express = require('express');
const router = express.Router();
const fs = require('fs')
const pdfParse = require("pdf-parse");
// const PDFParser = require("pdf2json");
const path = require('path');
const mongoose = require('mongoose');
const Grid = require('gridfs-stream');
const multer = require('multer');
var { MongoClient } = require('mongodb');
var assert = require('assert');
const mongodb = require('mongodb');
// const AWS = require('aws-sdk');

const FileModel = require("../models/File");
const Book = require("../models/Book");
const booksJson = require("../json_files/books.js");
var filePath2 = path.join(__dirname, 'The_Adventures_of_Sherlock_Holmes.pdf');
const dbConfig = require("../db");
const upload = require('../upload')


const saveBookMap = (req, res) => {
  booksJson.generalBooks.map((book) => {
    saveBook(book);
  })
}

const saveBook = async (book) => {
  return new Promise(async (resolve, reject) => {
    try {
      let newBooks = new Book({
        title: book.title,
        author: book.author,
        country: book.country,
        language: book.language,
        pages: book.pages,
        year: book.year,
        price: book.price,
        key: book.key
      })
      var returnedFile;
      if (book.file) {
        let filePath = book.file;
        returnedFile = await saveBooksJob(filePath, book.title)
      }
      if (returnedFile?._id) {
        newBooks.file = returnedFile?._id;
        Book.findOne({
          title: book.title
        }).then(book => {
          if (book) {
            reject("Book already exists")
          } else {
            newBooks.save().then(bookResp => {
              return bookResp
            })
          }
        }).catch(e => {
          console.log('Error happend while trying to save book: ', e)
        })
      } else {
        console.log("Book can't be saved")
      }
    } catch (err) {
      console.log('Error for save book function: ', err)
    }
  })
}

const saveBooksJob = async (path, title) => {
  return new Promise(async (resolve, reject) => {
    let dataBuffer = fs.readFileSync(path);
    await pdfParse(dataBuffer).then(async (data) => {
      let savedFileDetail = await savePdfFile(data, title)
      resolve(savedFileDetail)
    }).catch(function (error) {
      console.log("error:", error)
      reject(error)
    })
  })
}

const savePdfFile = (data, name) => {
  return new Promise((resolve, reject) => {
    var fileVar = new FileModel({
      name: name,
      file: data.text
    });
    FileModel.findOne({
      name
    }).then(fileData => {
      if (fileData) {
        reject("File already exists")
      } else {
        fileVar.save().then(resp => {
          resolve(resp)
        })
      }
    }).catch(filesaveerr => {
      reject(filesaveerr)
    })
  })
}

router.post('/save-book', async (req, res, cb) => {
  try {
    saveBookMap()
  } catch (err) {
    console.log("err :", err)
  }
});

router.get('/get-books', (req, res) => {
  Book.find({}).populate('file').exec((err, posts) => {
    return res.status(200).json(posts)
  })
})

router.get('/get-book-by-id/:id', (req, res) => {
  Book.findById(req.params.id).populate("file").exec((err, docs) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(docs)
    }
  });
})


/* Unused for now */

// let uploadFunc = async (req, res) => {
//   await upload(req, res, (err) => {
//     if (err) {
//       console.log("error in upload function:", err)
//     } else {
//       console.log("got res from upload:", req.files)
//       if (req.files.length <= 0) {
//         return res
//           .status(400)
//           .send({ message: "You must select at least 1 file." });
//       }
//       db.close();
//       return res.status(200).send({
//         message: "Files have been uploaded.",
//       });
//     }
//   });
// }

//Function to save Profile picture using GridFS

router.post('/save-image', async (req, res, next) => {
  try {
    MongoClient.connect(dbConfig.DB, async (err, db) => {
      assert.equal(null, err);
      await upload(req, res, (err) => {
        if (err) {
          console.log("error in upload function:", err)
        } else {
          console.log("got res from upload:", req.files)
          if (req.files.length <= 0) {
            return res
              .status(400)
              .send({ message: "You must select at least 1 file." });
          }
          db.close();
          return res.status(200).send({
            message: "Files have been uploaded.",
          });
        }
      });
    })
  } catch (error) {
    console.log(error);
    if (error.code === "LIMIT_UNEXPECTED_FILE") {
      return res.status(400).send({
        message: "Too many files to upload.",
      });
    }
    return res.status(500).send({
      message: `Error when trying upload many files: ${error}`,
    });
  }
});

//================================================================================

// const mongoURI = process.env.MONGO_URI;
const conn = mongoose.createConnection(dbConfig.DB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

let gfs;
conn.once('open', () => {
  gfs = new mongoose.mongo.GridFSBucket(conn.db, {
    bucketName: dbConfig.bucketName,
  });
});

router.get("/get-profile-image", (req, res) => {
  try {
    gfs.find({}).limit(1).sort({ $natural: -1 }).toArray((err, fileResponse) => {
      if (err) {
        console.log("err in get profile :", err);
        res.status(400).send({
          message: err,
        });
      } else {
        if (fileResponse.length > 0) {
          let value = gfs.openDownloadStreamByName(fileResponse?.[0]?.["filename"]).pipe(res);
          return value.data
        } else {
          res.status(400).send({ message: 'no records found! ' })
        }
      }
    })
  } catch (e) {
    console.log(e)
  }
});


//obsolete function to store images in Aws s3- but not in use right now
//=========================================================================
// const s3 = new AWS.S3({
//   accessKeyId: "AKIA36UL6TPF7W2F2NN5",
//   secretAccessKey: "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"
// });
// router.post('/upload-image', async (req, res) => {
//   try {
//     let imagePath = path.join(__dirname, "../public/images/Pride-Prejudice.jpg");
//     const fileContent = fs.readFileSync(imagePath);

//     // Setting up S3 upload parameters
//     const params = {
//       Bucket: 'aws-fileuploads',
//       Key: 'Pride-Prejudice.jpg', // File name you want to save as in S3
//       Body: fileContent
//     };

//     // Uploading files to the bucket
//     s3.upload(params, function (err, data) {
//       if (err) {
//         throw err;
//       }
//       console.log(`File uploaded successfully. ${data.Location}`);
//     });

//   } catch (error) {
//     console.log(error);
//     return res.status(400).send({
//       message: "error saving image:", error,
//     });
//   }
// })

//==========================================================================


module.exports =
  router



